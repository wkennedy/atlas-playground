<div type="HEADER">

**A9 Consulting Group - Atlas Playground Operating Guide**

</div>

![](atlasplayground.png){width="665" height="220"}\
\
\

\
\

\
\

\
\

\
\

\
<span style="font-style: normal"><span style="font-weight: normal">Atlas
Playground was created as a solution to the problem of providing a
highly-available Atlassian Development environment and services on Linux
in a virtualized environment.</span></span>\
\
\

Overview {#overview .western}
========

\
\
<span style="font-style: normal"><span style="font-weight: normal">Atlas
Playground provides the Atlassian SDK and supporting services including
SMB volume shares and DRBD in an Active/Passive HA
configuration.</span></span>\
\
<span style="font-style: normal"><span
style="font-weight: normal">LinuxHA provides the HA stack for the
project, and Heartbeat is used to drive the HA resources. Among these
are management of the DRBD volume for the Atlassian SDK, a VIP to serve
the filesystem and the smbd daemon.</span></span>\
\
<span style="font-style: normal"><span style="font-weight: normal">DRBD
provides a distributed filesystem for Atlassian Tools, and takes
advantage of LVM to provide flexible volumes.</span></span>\
\
<span style="font-style: normal"><span
style="font-weight: normal">Fencing is not a component of this
implementation.</span></span>\
\
\

BitBucket {#bitbucket .western}
=========

Access the BitBucket project at

[https://bitbucket.org/a9group/Atlas
Playground](https://bitbucket.org/a9group/atlas-playground)

Working with Git is beyond the scope of this document. Follow your
preferred workflow to clone the repository locally with 'git clone' or
use BitBucket's interface to fork the Master branch.

No formal style manual exists. One may emerge, but there is no
expectation for public contributions to co-exist from a style
perspective.

The BitBucket sources may be used to build a Debian version of the
appliance, using your preferred Debian bootstrapping procedure;
debbootstrap, network installation, or as an overlay to a pre-configured
VM.

**SUSE Studio and Kiwi** {#suse-studio-and-kiwi .western style="page-break-after: avoid"}
========================

We took advantage of SUSE Studio to rapidly deploy the basic Linux
operating system, including the necessary prerequisites, per Atlassian's
guidelines.

A byproduct of Studio is the Kiwi configuration file that can be used to
rebuild derivative appliances, or modify the base build configuration to
build iterations of an existing image.

Deployment {#deployment .western}
----------

We have broken the deployment operation down to four phases:

\
\

**Phase 1**: The import of the OVF configuration file and backing store.
We also perform some basic configuration, like setting up the network
and virtual filesystems that will be connected to DRBD.

NOTE: This is done for both servers, Hot and Warm

\
\

**Phase 2**: Configuration of the Server Roles – which server is Active,
and which remains Passive.

\
\

**Phase 3**: Testing. We put the machine through a few tests; checking
service failover, DRBD control handover, and the basics of fencing.

\
\

**Phase 4:**<span style="font-weight: normal"> Creation of a 'utility
node'. This third node will allow you to manage the HA cluster and
provide a spare environment to be swapped in. It also provides an
out-of-cluster environment from which to issue commands.</span>

Phase 1 {#phase-1 .western}
-------

#### Import OVF\
<span style="font-style: normal"><span style="font-weight: normal">Import OVF build from SUSE Studio</span></span>\
\
<span style="font-style: normal"><span style="font-weight: normal">There are several differences between the Studio download and a ready-to-run Atlas Playground deployment. This guide attempts to walk you through the process.</span></span>\
<span style="font-style: normal"><span style="font-weight: normal">Suse Studio delivers a compressed OVF archive, including the VM description and OS disk image. Extract this file and import to your hypervisor of choice.</span></span> {#import-ovf-import-ovf-build-from-suse-studio-there-are-several-differences-between-the-studio-download-and-a-ready-to-run-atlas-playground-deployment.-this-guide-attempts-to-walk-you-through-the-process.-suse-studio-delivers-a-compressed-ovf-archive-including-the-vm-description-and-os-disk-image.-extract-this-file-and-import-to-your-hypervisor-of-choice. .western}

This section is also a good starting point if you need to build
additional Atlas Playground pairs for Dev/Test, or to build a Disaster
Recovery environment.

**Login**\
\
<span style="font-style: normal"><span style="font-weight: normal">Login
as root with</span></span>\
\
<span style="font-style: normal"><span
style="font-weight: normal">username: root</span></span>\
<span style="font-style: normal"><span
style="font-weight: normal">password: linux</span></span>

#### Basic Configuration {#basic-configuration .western}

Network\
<span style="font-style: normal"><span style="font-weight: normal">Three
network interfaces should be available:</span></span>

1.  **Public**\
    <span style="font-style: normal"><span
    style="font-weight: normal">Link over which the SAMBA service
    is available. Also serves as ssh and package
    update connection.</span></span>
2.  **DRBD**\
    <span style="font-style: normal"><span
    style="font-weight: normal">Dedicated internal network specifically
    for DRBD sync; e.g., 192.168.9.n/24</span></span>
3.  **Heartbeat**\
    <span style="font-style: normal"><span
    style="font-weight: normal">Dedicated network for heartbeat
    communication; e.g., 192.168.10.n/24</span></span>

Disk\
<span style="font-style: normal"><span style="font-weight: normal">The
ideal implementation provides mirrored storage backing the LVM for DRBD.
The current iteration relies on the mirrored volumes for redundancy.
There are multiple paths which can be taken to provide fault tolerance
at the block level:</span></span>\
\
<span style="font-style: normal">**LVM Mirroring**</span>

Utilize a separate physical device to provide a hardware mirror for LVM.
Requires N+2 disks.

#### LVM automatic snapshots {#lvm-automatic-snapshots .western}

##### \
<span style="font-style: normal"><span style="font-weight: normal">Incorporate a cron-driven task to create volume snapshots. Recommended practice for preventing against disk invalidation errors occuring DRBD synchronizations.</span></span>\
<span style="font-style: normal"><span style="font-weight: normal">See: </span></span>[<span style="text-decoration: none"><span style="font-style: normal">**http://www.drbd.org/users-guide/s-lvm-snapshots.html**</span></span>](http://www.drbd.org/users-guide/s-lvm-snapshots.html)\
\
<span style="font-style: normal">**VMware Hardware mirroring**</span> {#incorporate-a-cron-driven-task-to-create-volume-snapshots.-recommended-practice-for-preventing-against-disk-invalidation-errors-occuring-drbd-synchronizations.-seehttpwww.drbd.orgusers-guides-lvm-snapshots.html-vmware-hardware-mirroring .western}

\
<span style="font-style: normal"><span
style="font-weight: normal">Utilize facilities within VMware for
automatic backup and enhanced storage allocation</span></span>\
\
<span style="font-style: normal">**Linux Kernel-based Software
mirroring**</span>

\
<span style="font-style: normal"><span style="font-weight: normal">Use
mdadm or raidtools to create and manage a software RAID.</span></span>

Preparing volumes\
\
**<span style="font-style: normal">Allocate within VMware</span>**\
<span style="font-style: normal"><span
style="font-weight: normal">Create or attach a virtual block device to
the guest.</span></span>\
\
**<span style="font-style: normal">Configure within OS</span>**\
<span style="font-style: normal"><span style="font-weight: normal">Atlas
Playground appliance expects a set of LVM2 volumes to be
available:</span></span>

\
<span style="font-style: normal">**Physical Volume**</span>\
<span style="font-style: normal"><span style="font-weight: normal">This
will change depending on the number of disks available to the appliance,
but by default '/dev/sda' is used for the operating system, and
'/dev/sdb' as the dedicated Physical Volume (pv) which is later
allocated to the Volume Group (vg) 'drbdsamba0vg'</span></span>\
<span style="font-style: normal"><span style="font-weight: normal">To
prepare LVM volumes for DRBD, run as root:</span></span>

``` {.code-western}
sh ha_init.sh
```

\
<span style="font-style: normal"><span style="font-weight: normal">From
/root directory.</span></span>\
\
<span style="font-style: normal"><span
style="font-weight: normal">Manual:</span></span>

``` {.code-western}
pvcreate /dev/sdb
```

##### **Volume Group** {#volume-group .western style="font-style: normal; page-break-after: avoid"}

\
\

This is the disk group upon which the logical volume 'drbdsamba0lv'
resides. The Volume Group may be expanded to include additional capacity
incorporated from additional Physical Volumes or extensions to the
PV/PVs already in use. 

Automatic Creation:

The ha\_init.sh script will remove any preexisting volumes on the
specified disk. Re-run this script as necessary to build the desired
disk onfiguration. Run as root user from '/root/':

``` {.code-western}
sh ha_init.sh
```

<span style="font-style: normal"><span
style="font-weight: normal">Manual:</span></span>

``` {.code-western}
vgcreate drbdsamba0vg /dev/sdb
```

****<span style="font-style: normal">Logical Volume</span>****

\
\

<span style="font-style: normal"><span style="font-weight: normal">The
Logical Volume 'drbdsamba0lv' upon which the device for DRBD
(samba0/drbd0) is created.</span></span>\
\
\

<span style="font-style: normal"><span style="font-weight: normal">Note:
The Linux filesystem for SAMBA is created on 'samba0' after preparation
and presentation to the OS through DRBD.</span></span>\
\
\

``` {.western style="margin-bottom: 0.2in; border: none; padding: 0in; widows: 2; orphans: 2"}
Automatic:
run as root from '/root/':
sh ha_init.sh
```

**Manual:**

``` {.code-western}
lvcreate -L 28640M -n drbdsamba0lv drbdsamba0vg
```

\
**<span style="font-style: normal">DRBD</span>**\
\
<span style="font-style: normal"><span style="font-weight: normal">DRBD
setup is still a manual process, and must be carried out after creating
or destroying the LVM configuration.</span></span>\
\
<span style="font-style: normal"><span style="font-weight: normal">Bring
up both nodes. Each must be accessible via the DRBD interface as well as
Public for SSH.</span></span>

-   **Verify the availability of the DRBD device:**

``` {.code-western style="margin-left: 0.49in"}
cat /proc/drbd
```

-   <span style="font-style: normal">**From 'hot' node, create the DRBD
    Metadata**</span>\
    <span style="font-style: normal"><span
    style="font-weight: normal">Run as root:</span></span>

``` {.code-western}
drbdadm create-md samba0
```

<span style="font-style: normal"><span style="font-weight: normal">Also
on the 'hot' node, </span></span><span
style="font-style: normal">**initialize the syncronization
process.**</span>

Run as root:

``` {.code-western}
drbdadm primary all
```

-   **Verify the status of the DRBD device as Primary/Inconsistant:**

\
\

``` {.code-western}
cat /proc/drbd
```

##### Network Time Protocol {#network-time-protocol .western}

\
\

Accurate synchronization between nodes is essential, and requires NTP
configuration.

<span style="font-style: normal"><span
style="font-weight: normal">Remove Local clock entry (127.x.x.x) and
replace in /etc/ntp.conf to include your preferred clock
source.</span></span>\
\
\
\

-   **Force clock sync:**

``` {.code-western style="margin-left: 0.49in"}
/etc/init.d/ntp restart
```

-   ``` {.western style="border: none; padding: 0in; font-style: normal; widows: 2; orphans: 2"}
    Check time
    ```

``` {.western style="margin-left: 0.49in; border: none; padding: 0in; font-style: normal; font-weight: normal; widows: 2; orphans: 2"}
From 'hot', as root, run:
date ; ssh warm date
```

### Phase 2 {#phase-2 .western}

#### \
Configure Roles {#configure-roles .western}

<span style="font-style: normal"><span style="font-weight: normal">\
</span></span>**<span style="font-style: normal">**Hot**</span>**<span
style="font-style: normal"><span style="font-weight: normal">\
</span></span><span style="font-style: normal"><span
style="font-weight: normal">Hot role is assumed on the host where
'ha\_init.sh' is run. This host may also be cloned from an OVF export,
and reconfigured with ha\_init.sh, or with a manual process to
reconfigure the hostname and IP addresses assigned.\
This guide gives 'warm' the final octet of .11</span></span>

<span style="font-style: normal"><span style="font-weight: normal">\
</span></span>**<span style="font-style: normal">**Warm**</span>**<span
style="font-style: normal"><span style="font-weight: normal">\
</span></span><span style="font-style: normal"><span
style="font-weight: normal">Warm role is inherited after preparing a
Atlas Playground with 'ha\_init.sh' and propagating with
'ha\_synchaconfig.sh'</span></span>

This guide gives 'warm' the final octet of .12

\
\

### Phase 3 {#phase-3 .western}

#### \
Test heartbeat\
Ensure the heartbeat daemon process is running {#test-heartbeat-ensure-the-heartbeat-daemon-process-is-running .western}

``` {.code-western style="margin-left: 0.49in"}
service heartbeat status
```

\
Should read 'running'

#### \
Test DRBD\
Ensure the DRBD module is loaded {#test-drbd-ensure-the-drbd-module-is-loaded .western}

``` {.code-western style="margin-left: 0.49in"}
lsmod | grep drbd
```

#### \
Ensure the DRBD process is running {#ensure-the-drbd-process-is-running .western}

``` {.code-western style="margin-left: 0.49in"}
service drbd status
```

\
Should read 'running'

##### \
Test SAMBA\
Ensure the SMB process is running {#test-samba-ensure-the-smb-process-is-running .western}

``` {.code-western style="margin-left: 0.49in"}
service smb status
```

\
Should read 'running'

### Clone Hot node to Warm {#clone-hot-node-to-warm .western}

\
Atlas Playground needs a hot and warm node to function.\
\
Clone the configured hot VM, including the volumes attached for DRBD.\
\
Boot in single-user mode by issuing

``` {.code-western style="margin-left: 0.49in"}
single
```

at the GRUB prompt. This will bring the system up for a single user,
without activating the network interfaces.\
\
Run

``` {.code-western style="margin-left: 0.49in"}
yast2 lan
```

<span style="font-style: normal"><span style="font-weight: normal">This
provides a console interface to reconfigure the each of the assigned
network interfaces for DRBD, Heartbeat, and the Public network.\
\
The hostname must also be changed at this time. Find this by selecting
the 'Hostname' tab in Yast.\
\
Reboot the new warm node, and test connectivity from the hot node via
ssh connection to each interface.\
\
This process should also be followed to create a Util node for testing
HA failover. This node may also be configured to provide monitoring with
Nagios.\
\
The </span></span>[<span style="text-decoration: none"><span
style="font-style: normal">**'ha\_hatest.sh'**</span></span>](http://www.sambha.openlpos.org/static.php?page=static120512-161944)<span
style="font-style: normal"><span style="font-weight: normal"> script can
be used to perform validation testing at this stage from the resulting
Utility node.</span></span>

<span style="font-style: normal"><span
style="font-weight: normal">Download from BitBucket:
</span></span>[https://](https://bitbucket.org/a9group/atlas-playground/root/util/fs_hatest.sh)

#### Test failover {#test-failover .western}

\
\
This procedure assumes the Warm node has been populated by following the
Clone procedures outlined above.

<span style="font-style: normal"><span style="font-weight: normal">From
the Util node, execute the ha\_hatest.sh script, available on in the
Atlas Playground project on BitBucket, here:
</span></span>[http://bitbucket.org/a9group/Atlas
Playground/ha\_hatest.sh](https://github.com/juddy/SAMBHA/blob/master/overlay/util/fs_hatest.sh)

\
Webmin may also be used to take control of Heartbeat's resources with
the Heartbeat module, under the “Cluster” panel.

\
Manually forcing failover may be done from any node, using the following
methods:

#### Standby: {#standby .western}

Force the current host to submit it's resources to the opposing host.

As root:

``` {.code-western style="margin-left: 0.49in"}
/usr/share/heartbeat/hb_standby ; tail -f /var/log/ha-log
```

This should show the transfer of control of the HA resources to the
other node.

### Phase 4\
\
Create Utility Node {#phase-4-create-utility-node .western}

\
A utility and test node cloned from a 'warm' host can be used to monitor
and test the performance of Atlas Playground.\
\
A test application is available to test failover through a number of
processes; 'ha\_hatest.sh'\
<span style="font-style: normal"><span style="font-weight: normal">\
Run ha\_hates</span></span><span style="font-style: normal"><span
style="font-weight: normal">t.sh from a Utility node to perform a suite
of tests to verify HA operability.</span></span>

The script seen below (ha\_hatest.sh on util) performs three tests on
each host: a forced reboot, a forced failover, and forced failback.  I
have run it in a few different permutations, and you can see several
functions that can be called as tests; force\_reboot, force\_failover,
stop\_heartbeat, force\_disconnect, force\_standby, etc.

The execution mechanism runs through 30 calls to one host (beginning
with hot), performing one of the tests at a specified interval,
currently set as 10, 20, and 25 seconds.  Every 30 seconds, three pieces
of data are written to a file in /mnt on util, which is the SAMBA share.
 A timestamp, a marker, and the name of the test being executed (if
any).  This logfile is written as 'ha-test.out.YYYYmmdd'.

``` {.western style="margin-left: 0.39in; margin-right: 0.39in; border: none; padding: 0in; font-style: normal; font-weight: normal; widows: 2; orphans: 2"}
#!/bin/bash## Tool for performing HA validation actions# Runs from Util node## wjkennedy@openlpos.org# Thu May 10 14:21:53 UTC 2012#########################INTERVAL=30TARGET="hot"FILE=/mnt/ha-test.out.$(date +%Y%m%d)echo "Checking for SMB mount.."mount | grep sambaif [ $? -eq 1 ]then        echo "No Samba mount found.."        echo " --> Mounting SMB volume.."        mount.cifs //samba/samba /mnt -o user=guest           if [ $? -eq 0 ]        then               echo "FATAL: Failed to mount SMB volume"               echo "Ensure heartbeat and VIP are available."               exit 1        fi        touch $FILE        if [ $? -ne 0 ]        then               echo "Failed to touch $FILE"               echo "FATAL: May not be able to remount or access the Share."               exit 1        fifiwrite_test_file(){FILE=/mnt/ha-test.out.$(date +%Y%m%d)echo "Sleeping for $INTERVAL between writes."echo "continuously writing to $FILE..."while true        do               date >> $FILE               echo "-----------------" >> $FILE               sleep $INTERVAL        done}get_status(){ssh $TARGET '/etc/init.d/drbd status'}swap_targets(){case $TARGET in                hot)               echo "Target already set to hot."               echo "Swapping to warm.." ; echo                TARGET=warm        ;;        warm)               echo "Setting target to hot."               TARGET=hot        ;;esac}force_standby(){ssh $TARGET "/usr/share/heartbeat/hb_standby"}stop_heartbeat(){ssh $TARGET "/etc/init.d/heartbeat stop"}force_secondary(){ssh $TARGET "drbdadm secondary all"}force_disconnect(){ssh $TARGET "drbdadm disconnect all"}force_reboot(){ssh $TARGET "telinit 6"}force_failover(){ssh $TARGET /usr/share/heartbeat/hb_standby&}force_failback(){ssh $TARGET "/usr/share/heartbeat/hb_standby"}test_host(){ITERATION=0TESTNUM=0while [ $ITERATION -le $INTERVAL ]do        ITERATION=$(expr "$ITERATION" + 1)        echo -n "  --> $ITERATION of $INTERVAL : " ; sleep 5 ; get_status               case $ITERATION in                              10)                       echo "Running test #1 - force reboot" | tee -a $FILE                       force_reboot               ;;               20)     echo "Running test #2 - force failover" | tee -a $FILE                       force_failover               ;;               25)     echo "Running test #3 - force failback" | tee -a $FILE                       force_failback               ;;               esac        TESTNUM=$(expr "$TESTNUM" + 1 )                               if [ $ITERATION -eq $INTERVAL ]        then               echo "Max reached - swapping targets.."               swap_targets               test_host        fidone}get_statuswrite_test_file &test_host
```

\
\
\

#### Support and Management {#support-and-management .western style="page-break-before: always"}

\
\

Disaster recovery, routine backup, and dev-test-prod workflows may be
achieved through the rotation of system snapshots managed in a number of
ways:

-   Routine Hypervisor-level exports
-   Rsync-driven replication from within Atlas Playground
-   Kiwi-driven replication
-   SystemImager replication

This guide assumes a block-level replicant is available for reassignment
as a utility or replacement node for a hot or warm failure, or in the
event that an additional pair should be made available. The steps are
essentially the same in each case. This guide uses example static IPv4
address assignment schemes, and should be substituted for a network
topology suited to your environment.

A flowchart to the appropriate processes described in this guide is
presented below. It may be used to create a 'Util' node, replace a
failed node, or as a checklist to prepare and verify operation of an
expanded Atlas Playground implementation.

\
The differences between 'hot' and 'warm nodes are minimal. Managing
Atlas Playground expansion through images ensures a rapid DR time and
flexibility between platforms and infrastructures.The Hot node should be
used as primary point of interaction, pushing configuration changes to
warm node via the builtin 'ha\_synchaconfig.sh'.

``` {.code-western style="margin-left: 0.49in; font-weight: normal"}
drbdadm primary all
```

##### Manual Reassignment Process Outlined {#manual-reassignment-process-outlined .western}

\
\

Assumes a cloned VM is available, and

1.  **Change hostname**\
    As root:

``` {.code-western style="margin-left: 0.49in"}
hostname warm
```

<span style="font-style: normal"><span style="font-weight: normal">\
</span></span><span style="font-style: normal">**Note: The same applies
to changing to 'hot' from warm. Use the opposing member.**</span>

1.  Check the /etc/hostname file to ensure the correct data; correct IP
    address for hostname – correcting for MAC address changes.

2.  Change interface IP addresses, verify hostname in YaST2 Hostname
    tab.\
    As root, run:

    ``` {.code-western style="font-style: normal; font-weight: normal"}
    yast2 lan
    ```

3.  Reconfigure each of the interfaces to use the correct IP address for
    the role.

    \
    **Hot**:

    1.  DRBD 192.168.10.11

    2.  Heartbeat192.168.9.11

**Warm**:

1.  DRBD192.168.10.12

2.  Heartbeat192.168.9.12

<span style="font-style: normal">**Alternately, edit the interface
definition files in /etc/sysconfig/network/ifcfg-eth0**</span><span
style="font-style: normal"><span style="font-weight: normal">\
</span></span>\
\

Verify

1.  Verify Heartbeat Resource availability

2.  Verify DRBD status\
    As root: run

    ``` {.code-western style="font-style: normal; font-weight: normal"}
     drbdadm status
    ```

3.  Verify VIP status\
    As root run

    ``` {.code-western style="font-style: normal; font-weight: normal"}
    ifconfig eth0:1
    ```

4.  Verify correct SAMBA service IP address is configured\
    Verify SAMBA status\
    As root: run

``` {.code-western}
smbstatus
```

##### Monitoring {#monitoring .western}

\
Each node has Nagios installed. Suggested configuration involves active
SMB checks on the opposing node and verification that the expected
/srv/samba mount for /dev/drbd0 is available\
Configuring other monitoring tools for monitoring Atlas Playground\
Verify SMB service availability on SAMBA VIP

##### Managing SAMBA data store {#managing-samba-data-store .western}

\
Atlas Playground manages the DRBD device made available through
Heartbeat as a resource for Heartbeat.\
\
No manual mount operations are required.\
\
If the filesystem must be made available to the Operating System outside
of Heartbeat's control, you must do the following:

``` {.code-western}
/etc/init.d/heartbeat stop
```

\
\
With DRBD:

``` {.code-western}
mount /dev/drbd0 /srv/samba
```

\
\
Without DRBD:

``` {.code-western}
mount /dev/drbdsamba0vg/drbdsamba0lv /srv/samba
```

<span style="font-style: normal"><span style="font-weight: normal">\
</span></span><span style="font-style: normal"><span
style="font-weight: normal">\
If this fails, be sure the block device housing the Physical Volumes for
each of the volumes in the group in which the drbdsamba0lv volume
exists\
\
For assistance with raw disk outside of DRBD control, contact your
support contact for Atlas Playground or </span></span><span
style="text-decoration: none"><span
style="font-style: normal">**william@a9group.net**</span></span><span
style="font-style: normal"><span style="font-weight: normal">\
</span></span>\
\

#### \
Modifying Heartbeat Resources {#modifying-heartbeat-resources .western}

Atlas Playground's Heartbeat resource file is located in
/etc/ha.d/samba.conf

\
\

