# Atlas Playground from A9 Consulting Group
[http://atlassian.a9group.net](http://atlassian.a9group.net)


## Info
I built Atlas Playground in an attempt to provide a ready-to-run Linux development environment for deployment in a broad series of scenarios:

- Desktop development in a VirtualMachine
- Deployment to an in-house Virtualization Framework (VMware, VirtualBox, Xen)
- Physical servers with dedicated resources, enabling multiple developers to access the same hardware
- In any of these scenarios, where High Availability is desired

Atlas Playground is based on openSUSE Leap 42.1 64-bit x86 and uses a number of HA management utilities developed by A9 Consulting Group to manage failover, storage and cluster features.

This release uses Atlassian's Plugin SDK version 6.2.7-1 and contains the [standard commands](https://developer.atlassian.com/docs/developer-tools/working-with-the-sdk/command-reference).

# Using this release

## Hypervisor Import
Download the OVF from the SUSE Studio site: [https://susestudio.com/a/LVy15n/a9-atlas-playground/download/ovf](https://susestudio.com/a/LVy15n/a9-atlas-playground/download/ovf)
 
Extract the archive containing the disk image file and configuration data.

Import the OVF file using the VirtualBox 'Import VM' feature. Other hypervisor platforms are supported. Follow the OVF import instructions for those products.

> OVF virtual machine (.ovf) Open Virtualization Format (OVF) is an open, standards-based format for virtual machines. A variety of hypervisors, including SUSE Cloud, VirtualBox, VMware ESX, IBM SmartCloud, and Oracle VM, support creating virtual machines by importing an .ovf file.
> 
> In order to use an OVF image in VMWare, you may require the ovftool from VMware. Due to licensing issues we cannot distribute the tools. Download links, installation instructions, and documentation are available at the VMware Developer Center.

## Launch and Login

Once imported, launch an instance of Atlas Playground.

Once online, you can login as 'atlas' or 'root'.

To get started, use the root account. This is required when you want to bring up the HA features; DRBD storage, execute a failover, etc.

To run the SDK commands, you can use root or atlas.

atlas:summit16!

root:linux

Once logged in, you may run any of the atlas-* commands from the SDK.

**Note:** This release provides only a vtty console, Xorg and desktop environment is not installed. *You may install a desktop and utilities of your choice. Run and use 'yast sw_single' to install the packages you require.*

----

## Configuration of version 0.0.11

### Software sources

* wkennedy_ openSUSE 13.1

URL
: http://susestudio.com/user_repos/erCgsGLN3E1V4U2WnquE/

Base system
: 13.1

Last update
: 2016-07-19 15:46:56 UTC

* openSUSE Leap 42.1 OSS

URL
: http://download.opensuse.org/repositories/openSUSE:/Leap:/42.1/standard/

Base system
: Leap_42.1

Last update
: 2016-07-19 08:20:34 UTC

* wkennedy_ openSUSE Leap 42.1

URL
: http://susestudio.com/user_repos/RN5CuCEKvYT0hr2HEpmS/

Base system
: Leap_42.1

Last update
: 2016-07-19 16:28:27 UTC

### Selected software

alsa-firmware, atlassian-plugin-sdk, atmel-firmware, dhcp, dhcp-client, dhcp-server, dhcp-tools, git, glibc-locale, grub, grub2, grub2-branding-openSUSE, gvim, iputils, ipw-firmware, iw, java-1_8_0-openjdk-devel, kernel-default, kernel-firmware, kiwi, kiwi-desc-isoboot, kiwi-desc-oemboot, kiwi-desc-vmxboot, kiwi-instsource, kiwi-media-requires, kiwi-templates, kiwi-test, kiwi-tools, less, mingetty, mysql-connector-java, net-tools, nfs-kernel-server, patterns-openSUSE-base, patterns-openSUSE-console, patterns-openSUSE-devel_basis, patterns-openSUSE-devel_C_C++, patterns-openSUSE-devel_kernel, patterns-openSUSE-enhanced_base, patterns-openSUSE-fonts, patterns-openSUSE-laptop, patterns-openSUSE-multimedia, patterns-openSUSE-yast2_basis, qemu, qemu-block-curl, qemu-guest-agent, qemu-linux-user, qemu-tools, rungetty, sax3, sudo, SuSEfirewall2, syslog-ng, systemd, systemd-sysvinit, terminus-bitmap-fonts, tig, tmux, traceroute, vim, vim-data, vim-plugin-colorschemes, wget, WindowMaker-devel, wireless-tools, wpa_supplicant, wpa_supplicant-gui, xkeyboard-config, xorg-cf-files, xorg-sgml-doctools, xorg-x11-devel, xorg-x11-util-devel, xtermset, yast2, yast2-dhcp-server, yast2-firstboot, yast2-iscsi-client, yast2-live-installer, yast2-nfs-client, yast2-nfs-server, yast2-sound, zd1211-firmware, zypper

###

Scripts

Run script at the end of the build
:

    #!/bin/bash -e
    #
    # This script is executed at the end of appliance creation.  Here you can do
    # one-time actions to modify your appliance before it is ever used, like
    # removing files and directories to make it smaller, creating symlinks,
    # generating indexes, etc.
    #
    # The 'kiwi_type' variable will contain the format of the appliance
    # (oem = disk image, vmx = VMware, iso = CD/DVD, xen = Xen).
    #

    # read in some variables
    . /studio/profile

    # read in KIWI utility functions
    . /.kconfig

Run script whenever the appliance boots
:

    #!/bin/bash
    #
    # This script is executed whenever your appliance boots.  Here you can add
    # commands to be executed before the system enters the first runlevel.  This
    # could include loading kernel modules, starting daemons that aren't managed
    # by init files, asking questions at the console, etc.
    #
    # The 'kiwi_type' variable will contain the format of the appliance (oem =
    # disk image, vmx = VMware, iso = CD/DVD, xen = Xen).
    #

    # read in some variables
    . /studio/profile

    if [ -f /etc/init.d/suse_studio_firstboot ]
    then
        echo "To run Atlassian products:"
        echo "Application | Default Port | Atlas launch command"
    echo
    echo  "Confluence | 1990 | atlas-run-standalone --product confluence"
    echo
    echo "JIRA | 2990 | atlas-run-standalone --product jira"
    echo
    echo "Fisheye/Crucible | 3990 | atlas-run-standalone --product fecru"
    echo
    echo "Crowd | 4990 atlas-run-standalone --product crowd"
    echo
    echo "Bamboo | 6990 atlas-run-standalone --product bamboo"
    echo
    echo "Stash | 7990 | atlas-run-standalone --product stash"
    fi

----
###  General

* Default locale

Language
: en_US.UTF-8

Keyboard Layout
: english-us

* Default time zone

Region
: Global

Time Zone
: UTC

* Network

Configuration
: dhcp

* Firewall

Enabled
: **false**

###Users and groups

atlas

UID (optional)
: 1000

Password
: summit14!

Group
: users

Home directory
: /home/atlas

Shell
: /bin/bash

root

UID (optional)
: 0

Password
: linux

Group
: root

Home directory
: /root

Shell
: /bin/bash

----
###Startup

* Default runlevel
: 3

###Server

* MySQL

Set up MySQL: **false**

* PostgreSQL
Set up PostgreSQL: **false**

###Desktop

* Automatic desktop user log in

Automatically log in user
: **false**

###Appliance

* Disk and memory

Memory ( OVF, VMware, and Xen )
: 512 MB

Disk size ( EC2, OVF, VMware, KVM, and Xen )
: 16 GB

System size ( Disk image )
: 0 GB

Swap partition ( Disk image )
: 512 MB

Enable extended memory (PAE)?
: **false**

* Logical Volume Manager

Configure LVM
: **false**

* Additional options

Add live installer to CDs and DVDs
: *true*

Enable Xen host mode
: **false**

Enable VMware CD-ROM support
: *true*

Enable support for UEFI secure boot
: **false**

----
## Command Reference

**atlas-clean** — atlas-clean [options] – Removes files from the project directory, that were generated during the build. (Runs mvn clean.) Passes all parameters straight through to Maven.

**atlas-cli** — atlas-cli [options] – Starts up a command line interface to your plugin running in the host application. After you change the plugin code, enter pi at the CLI prompt to install the updated plugin in the Atlassian application. (Runs mvn amps:cli.) Interpreted parameters: http-port, context-path, server, cli-port.

**atlas-clover** — atlas-clover [options] – Runs unit and integration tests, generating a code coverage report with Clover http://atlassian.com/clover. Passes all parameters straight through to Maven. Report is available in target/site/clover/index.html.

**atlas-compile** — atlas-compile [options] – Compiles the sources of your project. (Runs mvn compile.) Passes all parameters straight through to Maven.

**atlas-create-bamboo-plugin** — atlas-create-bamboo-plugin [options] – Creates an example of a Bamboo plugin, which you can adapt to suit your own plugin's needs. (Runs mvn bamboo:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-bamboo-plugin-module** — atlas-create-bamboo-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a Bamboo plugin module that you can adapt to suit your own plugin's needs. (Runs mvn bamboo:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-create-confluence-plugin** — atlas-create-confluence-plugin [options] – Creates an example of a Confluence plugin, which you can adapt to suit your own plugin's needs. (Runs mvn confluence:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-confluence-plugin-module** — atlas-create-confluence-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a Confluence plugin module that you can adapt to suit your own plugin's needs. (Runs mvn confluence:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-create-crowd-plugin** — atlas-create-crowd-plugin [options] – Creates an example of a Crowd plugin, which you can adapt to suit your own plugin's needs. (Runs mvn crowd:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-crowd-plugin-module** — atlas-create-crowd-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a Crowd plugin module that you can adapt to suit your own plugin's needs. (Runs mvn crowd:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-create-fecru-plugin** — atlas-create-fecru-plugin [options] – Creates an example of a FishEye or Crucible plugin, which you can adapt to suit your own plugin's needs. (Runs mvn fecru:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-fecru-plugin-module** — atlas-create-fecru-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a FishEye/Crucible plugin module that you can adapt to suit your own plugin's needs. (Runs mvn fecru:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-create-home-zip** — atlas-create-home-zip – Creates a test-resources zip of the current application's home directory. This zip file can then be pointed to in the AMPS productDataPath property to auto-populate application data during startup.

**atlas-create-jira-plugin** — atlas-create-jira-plugin [options] – Creates an example of a JIRA plugin, which you can adapt to suit your own plugin's needs. (Runs mvn jira:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-jira-plugin-module** — atlas-create-jira-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a JIRA plugin module that you can adapt to suit your own plugin's needs. (Runs mvn jira:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-create-refapp-plugin** — atlas-create-refapp-plugin [options] – Creates an example of a RefApp plugin, which you can adapt to suit your own plugin's needs. (Runs mvn refapp:create.) Interpreted parameters: artifact-id, group-id, version, package, non-interactive.

**atlas-create-refapp-plugin-module** — atlas-create-refapp-plugin-module [options] – Prompts you for plugin module type and related details, then creates an example of a plugin module for the RefApp that you can adapt to suit your own plugin's needs. (Runs mvn refapp:create-plugin-module.) Passes all parameters straight through to Maven.

**atlas-debug** — atlas-debug [options] – Runs the application in debug mode with your plugin installed. (Runs mvn amps:debug.) Interpreted parameters: version, container, http-port, context-path, server, jvmargs, log4j, test-version, sal-version, rest-version, plugins, lib-plugins, bundled-plugins, product, jvm-debug-port, jvm-debug-suspend.

**atlas-help** — atlas-help [--verbose] – Displays help text for the shell scripts incorporated into the Atlassian Plugin SDK. Interpreted parameters: verbose.

**atlas-install-plugin** — atlas-install-plugin [options] – Installs the plugin into a running application. (Runs mvn amps:install.) Interpreted parameters: http-port, context-path, server, username, password, plugin-key.

**atlas-integration-test** — atlas-integration-test [options] – Runs the integration tests for the plugin. (Runs mvn integration-test.) Interpreted parameters: version, container, http-port, context-path, server, jvmargs, log4j, test-version, sal-version, rest-version, plugins,lib-plugins, bundled-plugins, product, no-webapp, skip-tests.

**atlas-mvn** — atlas-mvn [options] – Allows you to execute any Maven command using the version of Maven bundled with your Atlassian Plugin SDK. (Runs mvn.) Passes all parameters straight through to Maven.

**atlas-package** — atlas-package[options] – Packages the plugin artifacts and produces the JAR. (Runs mvn package.) Passes all parameters straight through to Maven.

**atlas-release** — atlas-release [options] – Performs a release of the current plugin. (Runs mvn release:prepare release:perform.) Passes all parameters straight through to Maven.

**atlas-release-rollback** — atlas-release-rollback [options] – Rolls back a release of the current plugin. (Runs mvn release:rollback.) Passes all parameters straight through to Maven.

**atlas-run** — atlas-run [options] – Runs the application with your plugin installed. (Runs mvn amps:run.) Interpreted parameters: version, container, http-port, context-path, server, jvmargs, log4j, test-version, sal-version, rest-version, plugins, lib-plugins, bundled-plugins, product.

**atlas-run-cloud** — atlas-run-cloud [options] – Runs an Atlassian application with Connect and the Cloud dependencies installed. Interpreted parameters: application, maven-plugin-version.

**atlas-run-standalone** — atlas-run-standalone [options] – Runs an Atlassian application standalone, without a plugin project (that is, not requiring atlas-create-<product>-plugin). Interpreted parameters: version, container, http-port, context-path, server, jvmargs, log4j, test-version, sal-version, rest-version, plugins, lib-plugins, bundled-plugins, product.

**atlas-unit-test** — atlas-unit-test [options] – Runs the unit tests for your plugin. (Runs mvn test.) Passes all parameters straight through to Maven.

**atlas-update** — atlas-update [options] – Updates the Atlassian Plugin SDK.

**atlas-version** — atlas-version – Displays version and runtime information for the Atlassian Plugin SDK. (Runs mvn --version.)
