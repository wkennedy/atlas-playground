#!/bin/bash
#
#
# Atlas HA Tools for Atlas Playground
#
# william@a9group.net
# Wed Aug 27 18:20:17 MDT 2014
#
# ha_adddrbdvol.sh
# Tool for integrating additional storage into DRBD
#
############################################

MOUNTED=$(mount | awk '{print $1}' | grep '^/dev')

echo "Mounted filesystems:"
echo "$MOUNTED"


echo "------------------------------"
echo "Physical Volumes known to LVM:"
pvs
echo "------------------------------"
echo
echo "------------------------------"
echo "Volume Groups known to LVM:"
vgs
echo "------------------------------"
echo
echo "------------------------------"
echo "Logical Volumes known to LVM:"
lvs
echo "------------------------------"
echo

echo "------------------------------"
echo "Finding unallocated disks.."
blockdev --report | grep -ev $MOUNTED

